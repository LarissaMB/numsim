/*
 * Copyright (C) 2015   Malte Brunn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//------------------------------------------------------------------------------
#include "typedef.hpp"
#include "communicator.hpp"
#include "compute.hpp"
#include "geometry.hpp"
#include "parameter.hpp"
#include "grid.hpp"

#ifdef USE_DEBUG_VISU
#include "visu.hpp"
#endif // USE_DEBUG_VISU
// #ifdef USE_VTK
#include "vtk.hpp"
#include <sys/stat.h>
// #endif // USE_VTK


int main(int argc, char **argv) {

  // Create parameter and geometry instances with default values
  Communicator comm(&argc, &argv);
  Parameter param;
  Geometry geom(&comm);

  std::cout << "Please choose from the following cases" << std::endl;
  std::cout << "0 = Karman Vortex Street \n1 = Flow over a step \n2 = Natural convection (case 1)" << std::endl;
  std::cout << "3 = Natural convection (case 2) \n4 = Fluid Trap \n5 = Fluid Trap (switched temperatures) \n6 = Rayleigh Benard" << std::endl;
  std::cout << "If none of the above listed cases is chosen, the default case presented is driven-cavity." << std::endl;

  const char *filegeom;
  const char *fileparam;

  if(argc > 1){
    char inputcase = argv[1][0];
    switch (inputcase){
      case '0': 
        std::cout << "Starting Karman Vortex Street..." << std::endl;
        filegeom = "Geoms/karman.geom";
        fileparam = "Geoms/karman.param";
        param.Load(fileparam);  // Example a)
        geom.Load(filegeom);
        break;
      case '1': 
        std::cout << "Starting flow over a step..." << std::endl;
        filegeom = "Geoms/step.geom";
        fileparam = "Geoms/step.param";
        param.Load(fileparam);  // Example b)
        geom.Load(filegeom);
        break;
      case '2': 
        std::cout << "Starting natural convection (case 1)..." << std::endl;
        filegeom = "Geoms/NaturalConvection.geom";
        fileparam = "Geoms/NaturalConvection.param";
        param.Load(fileparam);  // Example c1)
        geom.Load(filegeom);
        break;
      case '3':
      std::cout << "Starting natural convection (case 2)..." << std::endl;
        filegeom = "Geoms/NaturalConvection.geom";
        fileparam = "Geoms/NaturalConvection2.param";
        param.Load(fileparam);  // Example c2)
        geom.Load(filegeom);
        break;
      case '4': 
        std::cout << "Starting fluid trap..." << std::endl;
        filegeom = "Geoms/FluidTrap.geom";
        fileparam = "Geoms/FluidTrap.param";
        param.Load(fileparam);  // Example d1)
        geom.Load(filegeom);
        break;
      case '5': 
        std::cout << "Starting fluid trap with switched temperatures..." << std::endl;
        filegeom = "Geoms/FluidTrap2.geom";
        fileparam = "Geoms/FluidTrap.param";
        param.Load(fileparam);  // Example d2)
        geom.Load(filegeom);
        break;
      case '6':
        std::cout << "Starting Rayleigh Benard example..." << std::endl;
        filegeom = "Geoms/RayleighBenard.geom";
        fileparam = "Geoms/RayleighBenard.param";
        param.Load(fileparam);  // Example e)
        geom.Load(filegeom);
        break;
      default: 
        // std::cout << "Starting default driven-cavity..." << std::endl;
        break;
    }
  }
  else{
    std::cout << "Starting default driven-cavity..." << std::endl;
    filegeom = "Geoms/default.geom";
    fileparam = "Geoms/default.param";
    param.Load(fileparam);
    geom.Load(filegeom);
  }
  

// Create the fluid solver
Compute comp(&geom, &param, &comm);

// #ifdef USE_VTK
  if (comm.getRank() == 0) {
    // check if folder "VTK" exists
    struct stat info;

    if (stat("VTK", &info) != 0) {
      system("mkdir VTK");
    }
  }
// #endif

// Create and initialize the visualization
#ifdef USE_DEBUG_VISU
  Renderer visu(geom.Length(), geom.Mesh());
  double ratio = geom.Length()[1]/geom.Length()[0];
  visu.Init(800/ comm.ThreadDim()[0], 800*ratio/ comm.ThreadDim()[1], comm.getRank() + 1);
#endif // USE_DEBUG_VISU

// #ifdef USE_VTK
  // Create a VTK generator;
  // use offset as the domain shift
  multi_real_t offset;
  offset[0] = comm.ThreadIdx()[0] * (geom.Mesh()[0] * (double)(geom.Size()[0] - 2));
  offset[1] = comm.ThreadIdx()[1] * (geom.Mesh()[1] * (double)(geom.Size()[1] - 2));
  VTK vtk(geom.Mesh(), geom.Size(), geom.TotalSize(), offset, comm.getRank(),
          comm.getSize(), comm.ThreadDim());

//------------------------------------------------------------------------------
//VTK::VTK(const multi_real_t &h, const multi_real_t &fieldlength,
      //const multi_real_t &globalFieldLength, const int &rank, const int &size,
      // multi_index_t fieldDims({1,1});
      // multi_real_t size;
      // size[0] = geom.TotalSize()[0];
      // size[1] = geom.TotalSize()[1];
  // VTK vtk(geom.Mesh(), size, size, 0, 1, fieldDims);
// #endif

#ifdef USE_DEBUG_VISU
  const Grid *visugrid;

  visugrid = comp.GetVelocity();
#endif // USE_DEBUG_VISU

  // Run the time steps until the end is reached
  while (comp.GetTime() < param.Tend()) {
#ifdef USE_DEBUG_VISU
    // Render and check if window is closed
    // Render with global min and global max prevents wrong coloring in p>2 ranks
    // , comm.gatherMin(visugrid), comm.gatherMax(visugrid)
    switch (visu.Render(visugrid)) {
    case -1:
      return -1;
    case 0:
      visugrid = comp.GetVelocity();
      break;
    case 1:
      visugrid = comp.GetU();
      break;
    case 2:
      visugrid = comp.GetV();
      break;
    case 3:
      visugrid = comp.GetP();
      break;
    default:
      break;
    };
#endif // USE_DEBUG_VISU

  vtk.Init("VTK/field");
  vtk.AddRank();
  vtk.AddCellField("Cell Velocity", comp.GetU(), comp.GetV());
  vtk.SwitchToPointData();
  vtk.AddPointField("Velocity", comp.GetU(), comp.GetV());
  vtk.AddPointScalar("Pressure", comp.GetP());
  vtk.AddPointScalar("Temperature", comp.GetT());
  vtk.Finish();

    // Run a few steps
    for (uint32_t i = 0; i < 9; ++i) {
      comp.TimeStep(false);
    }

    // suppress output on other nodes than rank 0
    bool printOnlyOnMaster = !comm.getRank();
    comp.TimeStep(printOnlyOnMaster);
  }

  return 0;
}
