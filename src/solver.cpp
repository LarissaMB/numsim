/*
 * Copyright (C) 2015   Malte Brunn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solver.hpp"
#include "iterator.hpp"
#include "geometry.hpp"
#include "communicator.hpp"
#include "grid.hpp"
//------------------------------------------------------------------------------
Solver::Solver(const Geometry *geom) : _geom(geom) {}
//------------------------------------------------------------------------------
Solver::~Solver() {}
//------------------------------------------------------------------------------
real_t Solver::localRes(const Iterator &it, const Grid *grid,
                        const Grid *rhs) const {
  return grid->dxx(it) + grid->dyy(it) - rhs->Cell(it);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
SOR::SOR(const Geometry *geom, const real_t &omega) : Solver(geom) {
  _omega = omega;
}
//------------------------------------------------------------------------------
SOR::~SOR() {}
//------------------------------------------------------------------------------
real_t SOR::Cycle(Grid *grid, const Grid *rhs) const {
  InteriorIterator it(_geom);
  real_t res = 0.0;
  real_t local;
  real_t _h =
      (_geom->Mesh()[0] * _geom->Mesh()[0] * _geom->Mesh()[1] *
       _geom->Mesh()[1] / (2.0 * (_geom->Mesh()[0] * _geom->Mesh()[0] +
                                  _geom->Mesh()[1] * _geom->Mesh()[1])));
  for (it.First(); it.Valid(); it.Next()) {
    local = localRes(it, grid, rhs);
    res += local * local;
    grid->Cell(it) += _omega * _h * local;
  }
  return res;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
RedOrBlackSOR::RedOrBlackSOR(const Geometry *geom, const real_t &omega)
    : SOR(geom, omega) {}
//------------------------------------------------------------------------------
RedOrBlackSOR::~RedOrBlackSOR() {}
//------------------------------------------------------------------------------
real_t RedOrBlackSOR::RedCycle(Grid *grid, const Grid *rhs) const {
  InteriorIterator it(_geom);
  real_t res = 0.0;
  real_t local;
  real_t _h =
      (_geom->Mesh()[0] * _geom->Mesh()[0] * _geom->Mesh()[1] *
       _geom->Mesh()[1] / (2.0 * (_geom->Mesh()[0] * _geom->Mesh()[0] +
                                  _geom->Mesh()[1] * _geom->Mesh()[1])));
  for (it.First(); it.Valid(); it.Next()) {
    local = localRes(it, grid, rhs);
    res += local * local;
    grid->Cell(it) += _omega * _h * local;
    it.Next();
  }
  return res;
}
//------------------------------------------------------------------------------
real_t RedOrBlackSOR::BlackCycle(Grid *grid, const Grid *rhs) const {
  InteriorIterator it(_geom);
  real_t res = 0.0;
  real_t local;
  real_t _h =
      (_geom->Mesh()[0] * _geom->Mesh()[0] * _geom->Mesh()[1] *
       _geom->Mesh()[1] / (2.0 * (_geom->Mesh()[0] * _geom->Mesh()[0] +
                                  _geom->Mesh()[1] * _geom->Mesh()[1])));
  it.First();
  for (it.Next(); it.Valid(); it.Next()) {
    local = localRes(it, grid, rhs);
    res += local * local;
    grid->Cell(it) += _omega * _h * local;
    it.Next();
  }
  return res;
}
