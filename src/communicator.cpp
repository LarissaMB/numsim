#include "communicator.hpp"
#include "geometry.hpp"
#include "iterator.hpp"

/** Communicator constructor; initializes MPI Environment
   *
   * \param [in] argc Number of arguments program was started with
   * \param [in] argv Arguments passed to the program on start
   */
  Communicator::Communicator(int *argc, char ***argv){
     MPI_Init(argc,argv);
     MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
     MPI_Comm_size(MPI_COMM_WORLD, &_size);
     int tdim_int[2] = {0,0};
     int tidx_int[2] = {0,0};
     int nDims = 2;
     // create subdomains by splitting the field
     MPI_Dims_create(_size, nDims, tdim_int);
     // check periodic
     int periods[nDims];
     periods[0] = 0;
     periods[1] = 0;

     // set virtual topology
     MPI_Comm comm_cart;
     MPI_Cart_create(MPI_COMM_WORLD, nDims, tdim_int, periods, 0, &comm_cart);
     // set coordinates
     MPI_Cart_coords(comm_cart, _rank, nDims, tidx_int);

     _tdim[0] = tdim_int[0];
     _tdim[1] = tdim_int[1];
     _tidx[0] = tidx_int[0];
     _tidx[1] = tidx_int[1];

  }

  /** Communicator destructor; finalizes MPI Environment
   */
  Communicator::~Communicator(){
     MPI_Finalize();
  }

  /** Returns the position of the current process with respect to the
   *  fields lower left corner
   */
  const multi_index_t &Communicator::ThreadIdx() const{
     return _tidx;
  }

  /** Returns the way the domain is partitioned among all processes
   */
  const multi_index_t &Communicator::ThreadDim() const{
     return _tdim;
  }

  /** Returns whether this process is a red or a black field
   */
  const bool &Communicator::EvenOdd() const{
     return _evenodd;
  }

  /** Gets the sum of all values and distributes the result among all
   *  processes
   *
   * \param [in] val The data over which the sum is to be calculated
   */
  real_t Communicator::gatherSum(const real_t &val) const{
     double value = val;
     double sum = 0;
     MPI_Allreduce(&value, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
     return sum;
  }

  /** Finds the minimum of the values and distributes the result among
   *  all processes
   *
   * \param [in] val The data over which to find the minimum
   */
  real_t Communicator::gatherMin(const real_t &val) const{
     double value = val;
     double min = 0;
     MPI_Allreduce(&value, &min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
     return min;
  }

  /** Finds the maximum of the values and distributes the result among
   *  all processes
   *
   * \param [in] val The data over which to find the maximum
   */
  real_t Communicator::gatherMax(const real_t &val) const{
     double value = val;
     double max = 0;
     MPI_Allreduce(&value, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
     return max;
  }

  /** Synchronizes ghost layer
   *
   * \param [in] grid  The values to sync
   */
  void Communicator::copyBoundary(Grid *grid) const{
     if(_tdim[0] != 1){
        copyLeftBoundary(grid);
        copyRightBoundary(grid);
     }
     if(_tdim[1] != 1){
        copyTopBoundary(grid);
        copyBottomBoundary(grid);
     }
  }

  /** Decide whether our left boundary is a domain boundary
   */
  const bool Communicator::isLeft() const{
     if(_tidx[0] == 0){
        return true;
     }
     else{
        return false;
     }
  }

  /** Decide whether our right boundary is a domain boundary
   */
  const bool Communicator::isRight() const{
     if(_tidx[0] == _tdim[0]-1){
        return true;
     }
     else{
        return false;
     }
  }

  /** Decide whether our top boundary is a domain boundary
   */
  const bool Communicator::isTop() const{
     if(_tidx[1] == _tdim[1]-1){
        return true;
     }
     else{
        return false;
     }
  }

  /** Decide whether our bottom boundary is a domain boundary
   */
  const bool Communicator::isBottom() const{
     if(_tidx[1] == 0){
        return true;
     }
     else{
        return false;
     }
  }

  /** Get MPI rank of current process
   */
  const int &Communicator::getRank() const{
     return _rank;
  }

  /** Get number of MPI processes
   */
  const int &Communicator::getSize() const{
     return _size;
  }

  /** Function to sync ghost layer on left boundary:
   *  send values of own left boundary to left neighbor and
   *  and receive values from his right boundary
   *
   *   ------------ ------------
   *  |           x|y           |
   *  |           x|y           |
   *  |           x|y           |
   *  |           x|y           |
   *  |           x|y           |
   *   ------------ ------------
   *
   *   y: values that are sent
   *   x: values that are received
   *
   * \param [in] grid  values whose boundary shall be synced
   */
  bool Communicator::copyLeftBoundary(Grid *grid) const{
	  MPI_Status status;
	  multi_index_t size = grid->getGeometry()->Size();
	  int s = size[1]; 
	  double data[s];
	  int snew;
	  int leftRank, rightRank; 
	  	  
	  if(isLeft()){
		  rightRank = _rank + (int)_tdim[1];

		  MPI_Recv(&snew, 1, MPI_INT, rightRank, 0 ,MPI_COMM_WORLD, &status);
		  double datanew[snew];
		  MPI_Recv(datanew, s, MPI_DOUBLE, rightRank, 0 ,MPI_COMM_WORLD, &status);

		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(1);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }		  
		  
		  return true;


	  }else if(isRight()){
		  leftRank = _rank - (int)_tdim[1];		  

		  Iterator it(grid->getGeometry());
		  it.First();
		  it = it.Right();

		  for (int i = 0; i < s; i++){
		  	  data[i] = grid->Cell(it);
		  	  it = it.Top();
		  }
		  
		  MPI_Send(&s, 1, MPI_INT, leftRank, 0, MPI_COMM_WORLD);
		  MPI_Send(data, s, MPI_DOUBLE, leftRank, 0, MPI_COMM_WORLD);

		  return true;
		  
	  }else{
		  leftRank = _rank - (int)_tdim[1];
		  rightRank = _rank + (int)_tdim[1];	
		  
		  Iterator it(grid->getGeometry());
		  it.First();
		  it = it.Right();
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Top();
		  }
		  
		  MPI_Sendrecv(&s, 1, MPI_INT, leftRank, 0, &snew, 1, MPI_INT, rightRank, 0 ,MPI_COMM_WORLD, &status);
		  double datanew[snew];
		  MPI_Sendrecv(data, s, MPI_DOUBLE, leftRank, 0, datanew, s, MPI_DOUBLE, rightRank, 0 ,MPI_COMM_WORLD, &status);
		  
		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(1);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  return true;  
	  }

  }

  /** Function to sync ghost layer on right boundary
   *  Details analog to left boundary
   *
   * \param [in] grid  values whose boundary shall be synced
   */
  bool Communicator::copyRightBoundary(Grid *grid) const{
	  MPI_Status status;
	  multi_index_t size = grid->getGeometry()->Size();
	  int s = size[1];
	  double data[s];
	  int snew;
	  int rightRank, leftRank;
	  
	  if(isRight()){
		  leftRank = _rank - (int)_tdim[1];
		  
		  MPI_Recv(&snew, 1, MPI_INT, leftRank, 0 ,MPI_COMM_WORLD, &status);
 		  double datanew[snew];
 		  MPI_Recv(datanew, s, MPI_DOUBLE, leftRank, 0 ,MPI_COMM_WORLD, &status);

		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(3);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  
		  return true;
		  
	  }else if(isLeft()){
		  rightRank = _rank + (int)_tdim[1];
		  
		  Iterator it(grid->getGeometry(), size[0]-2); 
		  		  
		  for (int i = 0; i < s; i++){
  			  data[i] = grid->Cell(it);
  			  it = it.Top();		  
  		  }

		  MPI_Send(&s, 1, MPI_INT, rightRank, 0, MPI_COMM_WORLD);
		  MPI_Send(data, s, MPI_DOUBLE, rightRank, 0, MPI_COMM_WORLD);

		  return true;
		  
	  }else{		  
		  leftRank = _rank - (int)_tdim[1];
		  rightRank = _rank + (int)_tdim[1];

		  Iterator it(grid->getGeometry(), size[0]-2); 
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Top();		  
		  }

		  MPI_Sendrecv(&s, 1, MPI_INT, rightRank, 0, &snew, 1, MPI_INT, leftRank, 0 ,MPI_COMM_WORLD, &status);
		  double datanew[snew];
		  MPI_Sendrecv(data, s, MPI_DOUBLE, rightRank, 0, datanew, s, MPI_DOUBLE, leftRank, 0 ,MPI_COMM_WORLD, &status);
		  
		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(3);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  return true;
	  }
	  
  }

  /** Function to sync ghost layer on top boundary
   *  Details analog to left boundary
   *
   * \param [in] grid  values whose boundary shall be synced
   */
  bool Communicator::copyTopBoundary(Grid *grid) const{
	  MPI_Status status;
	  multi_index_t size = grid->getGeometry()->Size();
	  int s = size[0]; 
	  double data[s];
	  int snew;
	  int topRank, bottomRank;
	  
	  if(isTop()){

		  bottomRank = _rank - 1;
		  
		  MPI_Recv(&snew, 1, MPI_INT, bottomRank, 0 ,MPI_COMM_WORLD, &status);
 		  double datanew[snew];
 		  MPI_Recv(datanew, s, MPI_DOUBLE, bottomRank, 0 ,MPI_COMM_WORLD, &status);

		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(0);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }		  
		  
		  return true;
		  
	  }else if(isBottom()){
		  
		  topRank = _rank + 1;
		  
		  Iterator it(grid->getGeometry(), size[0]*(size[1]-2));
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Right();
		  }

		  MPI_Send(&s, 1, MPI_INT, topRank, 0, MPI_COMM_WORLD);
		  MPI_Send(data, s, MPI_DOUBLE, topRank, 0, MPI_COMM_WORLD);

		  return true;
		  
	  }else{
		  topRank = _rank + 1;
		  bottomRank = _rank - 1;
		  
		  Iterator it(grid->getGeometry(), size[0]*(size[1]-2));
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Right();		  
		  }
		  
		  MPI_Sendrecv(&s, 1, MPI_INT, topRank, 0, &snew, 1, MPI_INT, bottomRank, 0, MPI_COMM_WORLD, &status);
		  double datanew[snew];
		  MPI_Sendrecv(data, s, MPI_DOUBLE, topRank, 0, datanew, s, MPI_DOUBLE, bottomRank, 0, MPI_COMM_WORLD, &status);
		  
		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(0);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  return true;
	  }
	  
  }

  /** Function to sync ghost layer on bottom boundary
   *  Details analog to left boundary
   *
   * \param [in] grid  values whose boundary shall be synced
   */
  bool Communicator::copyBottomBoundary(Grid *grid) const{
	  MPI_Status status;
	  multi_index_t size = grid->getGeometry()->Size();
	  int s = size[0];
	  double data[s];
	  int snew;
	  int topRank, bottomRank;
	  
	  if(isBottom()){
		  topRank = _rank + 1;

		  MPI_Recv(&snew, 1, MPI_INT, topRank, 0 ,MPI_COMM_WORLD, &status);
 		  double datanew[snew];
 		  MPI_Recv(datanew, s, MPI_DOUBLE, topRank, 0 ,MPI_COMM_WORLD, &status);

		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(2);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  return true;
		  
	  }else if(isTop()){
		  bottomRank = _rank - 1;

		  Iterator it(grid->getGeometry());
		  it.First();
		  it = it.Top();
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Right();
		  }
		  
		  MPI_Send(&s, 1, MPI_INT, bottomRank, 0, MPI_COMM_WORLD);
		  MPI_Send(data, s, MPI_DOUBLE, bottomRank, 0, MPI_COMM_WORLD);

		  return true;
		  
	  }else{
		  topRank = _rank + 1;
		  bottomRank = _rank - 1;
		  
		  Iterator it(grid->getGeometry());
		  it.First();
		  it = it.Top();
		  
		  for (int i = 0; i < s; i++){
			  data[i] = grid->Cell(it);
			  it = it.Right();		  
		  }

		  MPI_Sendrecv(&s, 1, MPI_INT, bottomRank, 0, &snew, 1, MPI_INT, topRank, 0 ,MPI_COMM_WORLD, &status);
		  double datanew[snew];
		  MPI_Sendrecv(data, s, MPI_DOUBLE, bottomRank, 0, datanew, s, MPI_DOUBLE, topRank, 0 ,MPI_COMM_WORLD, &status);

		  BoundaryIterator bit(grid->getGeometry());
		  bit.SetBoundary(2);
		  bit.First();
		  int i_array = 0;
		  while(bit.Valid()){
			  grid->Cell(bit) = datanew[i_array];
			  bit.Next();
			  i_array++;
		  }
		  return true;
	  }
  }
  