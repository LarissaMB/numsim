  #include "compute.hpp"
  #include "communicator.hpp"
  #include "geometry.hpp"
  #include "parameter.hpp"
  #include "grid.hpp"
  #include "iterator.hpp"
  #include "solver.hpp"
  #include <chrono>

  #define RB_SOR // choose solver, default is SOR if nothing is defined here
  
  /// Creates a compute instance with given geometry and parameter
  Compute::Compute(const Geometry *geom, const Parameter *param, const Communicator *comm){
    _geom = geom;
    _param = param;
	  _comm = comm;
    _t = 0.0;

    multi_real_t u_offset = {_geom->Mesh()[0], -0.5*_geom->Mesh()[1]};
    multi_real_t v_offset = {-0.5*_geom->Mesh()[0], _geom->Mesh()[1]};
    multi_real_t p_offset = {-0.5*_geom->Mesh()[0], -0.5*_geom->Mesh()[1]};
    multi_real_t vort_offset = {_geom->Mesh()[0], _geom->Mesh()[1]}; // "normal" grid
    multi_real_t stream_offset = {_geom->Mesh()[0], _geom->Mesh()[1]};

    _u = new Grid(_geom, u_offset);
    _v = new Grid(_geom, v_offset);
    _p = new Grid(_geom, p_offset);
    _T = new Grid(_geom, p_offset); // T_offset == p_offset
    _vort = new Grid(_geom, vort_offset);
    _stream = new Grid(_geom, stream_offset);
    _F = new Grid(_geom, u_offset);
    _G = new Grid(_geom, v_offset);
    _rhs = new Grid(_geom, p_offset);
    _tmp = new Grid(_geom, vort_offset);
    
    //determine solver 
    #ifdef RB_SOR
      _solver = new RedOrBlackSOR(_geom, _param->Omega());
    #else
      _solver = new SOR(_geom, _param->Omega());
    #endif

    // initialize
    _u->Initialize(0.0);
    _v->Initialize(0.0);
    _p->Initialize(0.0);
    _T->Initialize(_geom->getTemperature()[0]);
    _vort->Initialize(0.0);
    _stream->Initialize(0.0);
    _F->Initialize(0.0);
    _G->Initialize(0.0);
    _rhs->Initialize(0.0);
    _tmp->Initialize(0.0);

    // boundary conditions
    _geom->Update_P(_p);
    _geom->Update_T(_T);
    _geom->Update_U(_u);
    _geom->Update_V(_v);
    _geom->Update_U(_F);
    _geom->Update_V(_G);

    _t = 0.0;
    _dtlimit = _param->Re() * _geom->Mesh()[0] * _geom->Mesh()[0] *
               _geom->Mesh()[1] * _geom->Mesh()[1] /
               (2 * ((_geom->Mesh()[0] * _geom->Mesh()[0]) +
                     (_geom->Mesh()[1] * _geom->Mesh()[1])));
    if(_param->Pr() < 1){
      _dtlimit *= _param->Pr();
    }

  }
  /// Deletes all grids
  Compute::~Compute(){
    delete _u;
    delete _v;
    delete _p;
    delete _T;
    delete _vort;
    delete _stream;
    delete _F;
    delete _G;
    delete _rhs;
    delete _tmp;

  }

  /// Execute one time step of the fluid simulation (with or without debug info)
  // @ param printInfo print information about current solver state (residual
  // etc.)
  void Compute::TimeStep(bool printInfo){
    real_t dt = 2000.0;
    double u_max = _u->AbsMax();
    double v_max = _v->AbsMax();
    u_max = _comm->gatherMax(u_max);
    v_max = _comm->gatherMax(v_max);
    real_t dx = _geom->Mesh()[0];
    real_t dy = _geom->Mesh()[1];

    if(_param->Dt() > 0){
      dt = _param->Dt();
    }

    dt = _param->Tau() * std::min(dy/u_max,dx/v_max);
    dt = std::min(dt,_dtlimit);

    _geom->Update_U(_u);
    _geom->Update_V(_v);
    
    // VisualizeGrid(_u);
    // Timestep for T
    NewTemperature(dt);
    _geom->Update_T(_T);
    _comm->copyBoundary(_T);
    // F and G
    MomentumEqu(dt);
    //Synchronizes ghost layers, old update functions still needed
    _geom->Update_U(_F);
    _geom->Update_V(_G);
    _comm->copyBoundary(_F);
    _comm->copyBoundary(_G);

   // Calculate rhs
    RHS(dt); //(_dtlimit);

    // set residual and iteration limit
    real_t _epslimit = _param->Eps();
    index_t _itermax = _param->IterMax();
    index_t _iter = 0;
    real_t _epsc = _epslimit*2;
    bool rb = true;

    //Output infos
    if(printInfo){
      std::cout << "==============================================" << std::endl;
    	std::cout << "Time stepsize: " << _dtlimit << std::endl;
    	std::cout << "Current time step: " << _t << " of " << _param->Tend() << std::endl;
    }

    while (_iter < _itermax && _epsc > _epslimit){
      #ifdef RB_SOR
        //Red-Black-SOR
        
          for (index_t i = 0; i < 2; i++){
            if(rb){
              _epsc = _solver->RedCycle(_p, _rhs);
            }else{
              _epsc += _solver->BlackCycle(_p, _rhs);
            }
            rb = !rb;
            _comm->copyBoundary(_p);
            _geom->Update_P(_p);
          }

          _epsc = _comm->gatherSum(_epsc);
            
      #else
        //each grid loves SOR with orld boundary values
        _epsc = _solver->Cycle(_p,_rhs);
        //update boundaries of p
        _geom->Update_P(_p);
        _comm->copyBoundary(_p);
      #endif

      // still needed
      if(printInfo && _epsc <= _epslimit){
        std::cout << "Solver converged. Iterations: " << _iter+1 << std::endl;
      }
      
      _iter++;
    }
    
    //Compute new velocities from F and G and p
    NewVelocities(dt);
    //gray block: send velocities, old update functions still needed
    _geom->Update_U(_u);
    _geom->Update_V(_v);
    _comm->copyBoundary(_u);
    _comm->copyBoundary(_v);

    _t = _t + dt;

          /// Printinfo
      if(printInfo){
    	  std::cout << "Current residual: " << _epsc << std::endl;
          std::cout << "==============================================" << "\n" << std::endl;
      }

  }

/// for debug purposes
  void Compute::VisualizeGrid(const Grid *grid){
    Iterator it(_geom);
    it.First();
    while(it.Valid()){
      std::cout << std::setw (2) << grid->Cell(it) << "  ";
      it.Next();
      if(it%_geom->Size()[0] == 0){
        std::cout << std::endl;
      }
    }
    std::cout << std::endl;
  }

  /// Returns the simulated time in total
  const real_t &Compute::GetTime() const{
    return _t;
  }

  /// Returns the pointer to U
  const Grid *Compute::GetU() const{
    return _u;
  }
  /// Returns the pointer to V
  const Grid *Compute::GetV() const{
    return _v;
  }
  /// Returns the pointer to P
  const Grid *Compute::GetP() const{
    return _p;
  }
  /// Returns the pointer to T
  const Grid *Compute::GetT() const{
    return _T;
  }
  /// Returns the pointer to RHS
  const Grid *Compute::GetRHS() const{
    return _rhs;
  }

  /// Computes and returns the absolute velocity
  // the here used formula is based on simple calculation of vector length...
  const Grid *Compute::GetVelocity(){
    Iterator it(_geom);
    real_t du, dv;
    it.First();
    while(it.Valid()){
      // _tmp->Cell(it) = sqrt(_u->Cell(it)*_u->Cell(it) + _v->Cell(it)*_v->Cell(it));
      du = (_u->Cell(it) + _u->Cell(it.Left()))/2;
      dv = (_v->Cell(it) + _v->Cell(it.Down()))/2;
      _tmp->Cell(it) = sqrt(pow(du,2) + pow(dv,2));
      it.Next();
    }
    return _tmp;
  }
  /// Computes and returns the vorticity
  const Grid *Compute::GetVorticity(){
    Iterator it(_geom);
    it.First();
    while(it.Valid()){
      _vort->Cell(it) = _u->dy_r(it) - _v->dx_r(it);
      it.Next();
    }
    return _vort;
  }
  /// Computes and returns the stream line values
  const Grid *Compute::GetStream(){
    Iterator it(_geom);
    it.First();
    while(it.Valid()){
      if(it.Pos()[1] == 0){
        _stream->Cell(it) = _stream->Cell(it.Left()) - _v->Cell(it)*_geom->Mesh()[0];
      }
      else{
        _stream->Cell(it) = _stream->Cell(it.Down()) + _u->Cell(it)*_geom->Mesh()[1];
      }
      it.Next();
    }
    return _stream;
  }

  /// Compute the new velocites u,v
  void Compute::NewVelocities(const real_t &dt){
    // using F and G for calculating new velocities
    InteriorIterator it(_geom);
    while(it.Valid()){
      _u->Cell(it) = _F->Cell(it) - dt * _p->dx_r(it);
      _v->Cell(it) = _G->Cell(it) - dt * _p->dy_r(it);
      it.Next();
    }
  }
  /// Compute the new Temperature T
  void Compute::NewTemperature(const real_t &dt){
    InteriorIterator it(_geom);
    while(it.Valid()){
      _T->Cell(it) = _T->Cell(it) + dt * ((1.0/_param->Re()/_param->Pr()*(_T->dxx(it)+_T->dyy(it)))
                   - _T->DC_udT_x(it, _param->Alpha(), _u)
                   - _T->DC_vdT_y(it, _param->Alpha(), _v));
      it.Next();
    }
  }
  /// Compute the temporary velocites F,G
  void Compute::MomentumEqu(const real_t &dt){
    InteriorIterator it(_geom);
    while(it.Valid()){
      _F->Cell(it) = _u->Cell(it) + dt * ((_u->dxx(it) + _u->dyy(it)) / _param->Re()
        - _u->DC_udu_x(it, _param->Alpha())
        - _u->DC_vdu_y(it, _param->Alpha(), _v)) - dt * _param->Beta()
         * _param->Gx() * (_T->Cell(it) + _T->Cell(it.Right()))/2;
      _G->Cell(it) = _v->Cell(it) + dt * ((_v->dxx(it) + _v->dyy(it)) / _param->Re()
        - _v->DC_udv_x(it, _param->Alpha(), _u)
        - _v->DC_vdv_y(it, _param->Alpha())) - dt * _param->Beta()
         * _param->Gy() * (_T->Cell(it) + _T->Cell(it.Top()))/2;
      it.Next();
    }
  }
  /// Compute the RHS of the poisson equation
  void Compute::RHS(const real_t &dt){
    InteriorIterator it(_geom);
    while(it.Valid()){
      _rhs->Cell(it) = 1.0 / dt * (_F->dx_l(it) + _G->dy_l(it));
      it.Next();
    }
  }

